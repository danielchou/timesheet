﻿//requirejs paths methond:  路徑別名功能，減少重複輸入
requirejs.config({
    baseUrl: "../../bower_components/",
    paths: {
        "jquery": ['jquery/dist/jquery.min'],
        'vue': ['validator/vendor/vue-0.11.1'],
        'validator': ['validator/validator'],
        "moment": ["moment/min/moment.min"],
        "fullcalendar": ["fullcalendar/dist/fullcalendar.min"],
    }
});

//define(id?, dependencies?, factory);
define(["vue", "validator", "jquery", "moment", "fullcalendar"], function (Vue, validator, $, moment, fullcalendar) {
    var hostUrl = "http://danielchou-001-site1.smarterasp.net";
    var hostImageUrl = $("#imgHostUrl").val();
    $(function () {


        $('#calendar').fullCalendar({
            // put your options and callbacks here
            header: { left: 'today prev,next title', center: '', right: 'month,basicWeek,basicDay' },
            titleFormat: "YYYY MM",
            agenda: 'hh:mm{-hh:mm}',
            dayNamesShort: ['日', '一', '二', '三', '四', '五', '六'],
            editable: false,
            selectable: false,
            eventLimit: false, // allow "more" link when too many events
            events: "/Calendar/GetEvent",
            eventRender: function (e, element, view) {
                var arr = e.title.split("<b>");
                var evid = arr[0]
                    , user = arr[1]
                    , dressid = arr[2]
                    , data = arr[3]
                    , img = arr[4]
                    , insertdate = arr[5]
                    , note = arr[6]
                    , isPassed = arr[7]
                    , typ = e.className
                    , typo = { TryOn: "試穿", Landry: "清洗", WebTryOn: "網路預約" };
                element.append("<div class='ev-panel' data-evid='" + evid + "'><b class='ev-user'>" + user + "</b><b class='ev-typo'>" + typo[typ] + "</b><div class=ev-note>" + note + "</div><div class='ev-tyypp'>" + fn_dnsParserImages(data) + "</div></div>");
            },
            eventClick: function (calEvent, jsEvent, view) {
                var me = calEvent;
                console.log(me.id);
                location.href = "EditRsrvDate/" + me.id;
                /*console.log('Event: ' + calEvent.title,'Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY, view, view.name);
                 change the border color just for fun */
                //$(".ev-panel").css('background-color', '#3a87ad');
                //$(this).find(".ev-panel").css('background-color', 'brown');
            },
            dayClick: function (date, jsEvent, view) {
                //console.log(date);
            },
            loading: function (bool) {
                if (bool) { $(".fc-left").append("<b id='loading'>Loading...</b>"); $('#loading').show(); } else { $('#loading').remove(); }
            },
            windowResize: function (view) {
                //alert('The calendar has adjusted to a window resize');
            }
        });
        $(".fc-today-button").before($("#btnAddnew"));
        $("#loading,#btnAddnew").show();
    });

    $("#btnAddnew").on("click", function () {
        location.href = "/Calendar/NewRsrvDate";
    });

    /*反解圖型*/
    function fn_dnsParserImages(_dns_str) {
        dns_str = _dns_str;
        var obj_dns = {};
        var arr = _dns_str.split("%");
        for (var c in arr) {
            if (arr[c]) {
                var d = arr[c].split(","), s, f, n;
                if (d.length > 2) {
                    s = d[0];
                    f = d[1];
                    n = d[2];
                    /*console.log("dd",d.length, d, c, f, n);*/
                    obj_dns[c] = { c: s, f: f, n: n };
                }
            }
        }
        return fn_dnsDisplay(obj_dns);
    }

    function fn_dnsDisplay(_dns) {
        var ss = "", d, code;
        for (var c in _dns) {
            d = _dns[c];
            /*console.log(d);*/
            code = d.c.replace("|", "");
            ss += "<a href='" + hostUrl + "/Dress/ShowOneDressCode/" + code + "' target='_blank'><div style=\"background: #fff url('" + hostImageUrl + "/FileUpload/DressPhoto/120/" + d.f + "') no-repeat 30% 20%;\" class='dop-show' data-code='" + d.c + "'><span class=dr-title>" + d.n + "</span></div></a>";
        }
        return ss;
    }


});
