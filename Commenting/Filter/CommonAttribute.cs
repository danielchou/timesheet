﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Newtonsoft.Json;

namespace My.Attritube
{

    public class AllowCrossSiteJsonAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Origin", "*");
            base.OnActionExecuting(filterContext);
        }
    }

    public class JsonDateResult : JsonResult
    {
        public JsonDateResult() { }

        public override void ExecuteResult(ControllerContext context)
        {
            HttpResponseBase response = context.HttpContext.Response;
            response.ContentType = "application/json";
            if (ContentEncoding != null)
                response.ContentEncoding = ContentEncoding;
            if (Data != null)
            {
                JsonTextWriter writer = new JsonTextWriter(response.Output) { Formatting = Formatting.Indented };
                JsonSerializer serializer = JsonSerializer.Create(new JsonSerializerSettings());
                serializer.Serialize(writer, Data);
                writer.Flush();
            }
        }

    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class NoCacheAttribute : ActionFilterAttribute {
        public override void OnResultExecuting(ResultExecutingContext filterContext) {
            filterContext.HttpContext.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            filterContext.HttpContext.Response.Cache.SetValidUntilExpires(false);
            filterContext.HttpContext.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            filterContext.HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            filterContext.HttpContext.Response.Cache.SetNoStore();

            base.OnResultExecuting(filterContext);
        }
    }

    //public class TestAttr : ActionFilterAttribute 
    //{
    //    public string email { get; set; }

    //    public override void OnActionExecuting(ActionExecutingContext filterContext)
    //    {

    //        var dd = filterContext.ActionParameters["e"];
    //        Reserve e = (Reserve)dd;
    //        string email = (e == null) ? "" : e.email;
    //        string gid = (e == null) ? "" : e.gid;
    //        string url = "http://localhost:26924/IT_Service_Portal/Home/Home.aspx?gid=" + gid;

    //        if (String.IsNullOrEmpty(email))
    //        {
    //            filterContext.HttpContext.Response.Redirect(url);
    //        }
    //        else
    //        {
                
    //            //filterContext.Result = new RedirectToRouteResult(
    //            //    new RouteValueDictionary{
    //            //      {"controller","MeetingRoom"},
    //            //      {"action","Test"},
    //            //      {"returnUrl", filterContext.HttpContext.Request.RawUrl }
    //            //    });
    //        }
    //    }    
    //}
}