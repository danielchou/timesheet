﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using FormMaker.Models;
using System.Web.Routing;

namespace FormMaker.ActionFilter
{
    /// <summary>
    /// 檢查OneLab員工是否有Session[UserID]存在，若不存在導入到Portal去登入。
    /// </summary>
    public class CheckOneLabUserSessionAttribute : ActionFilterAttribute
    {
       

        public override void OnActionExecuting(ActionExecutingContext filterContext) {

            FormMakerEntities db;
            HttpContextBase hc = filterContext.HttpContext;
            string OLP_Url = ConfigurationManager.AppSettings["OLP"].ToString();
            string IsAnonymousForm = ConfigurationManager.AppSettings["AnonymousForm"].ToString();
            string UserID = "";
            string TSToken = "";

            if (IsAnonymousForm == "N")
            {

                if (hc.Session["UserID"] == null || hc.Session["TSToken"] ==null)
                {
                    //UserID = hc.Session["UserID"].ToString();
                    //TSToken = hc.Session["TSToken"].ToString();
                    //filterContext.Result = new RedirectResult(OLP_Url);


                    //When create a question go to here.....上prod記得要解開上面.,.
                    UserID = "ONE-0231";
                    filterContext.Controller.ViewData["UserID"] = UserID;
                }
                else
                {
                    UserID =hc.Session["UserID"].ToString();
                    filterContext.Controller.ViewData["UserID"] = UserID;
                }
            }
            else
            {
                //不記名的UserID,UserName都是亂數!
                filterContext.Controller.ViewData["UserID"] = System.Guid.NewGuid().ToString().Replace("-", "").Substring(0, 14);
                filterContext.Controller.ViewData["UserName"] = "Anonymous";
            }
        }
    }



    public class EffectivedDateFormAttribute : ActionFilterAttribute
    {
        public string FormSetID { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            FormMakerEntities db;
            ActionExecutingContext fc = filterContext;
            HttpContextBase hc = filterContext.HttpContext;
            string FsID = "";
            string UserID="";

            if (fc.ActionParameters.ContainsKey(FormSetID))
            {
                FsID = fc.ActionParameters[FormSetID].ToString();
            }
            else {
                fc.Controller.ViewData["Msg"] = "沒有單號就不能判別是否過期,....";
                fc.Result = new RedirectToRouteResult(new RouteValueDictionary{
                            {"controller", "FormMaker"},
                            {"action", "OddPage"}
                        });
            }

            using (db = new FormMakerEntities())
            {
                IEnumerable<FormSet> es = db.FormSets.Where(x => x.GID == FsID).ToList();
                if (es.Count() == 1)
                {
                    FormSet e = es.SingleOrDefault();
                    DateTime? start = e.StartDate;
                    DateTime? end = e.EndDate;
                    DateTime? now = DateTime.Now;
                    if (now > end || now < start)
                    {
                        fc.Controller.ViewData.Add("Msg", "目前該問卷時效已過!");
                        //fc.Controller.ViewData["Msg"]="目前該問卷時效已過!";

                        fc.Result = new RedirectToRouteResult(new RouteValueDictionary{
                            {"controller", "FormMaker"},
                            {"action", "OddPage"}
                        });
                    }
                    else {
                        fc.Controller.ViewData.Add("QuestionTitle",e.Name);

                        //這邊判斷是否已經輸入了...
                        if (hc.Session["UserID"] != null)
                        {
                            UserID = hc.Session["UserID"].ToString();
                            IEnumerable<UserData> us = db.UserDatas.Where(x => x.UserID == UserID && x.FormID == FsID).ToList();
                            FormSet fs = db.FormSets.Where(x => x.GID == FsID).SingleOrDefault();
                            if (us.Count() > 0)
                            {
                                UserData u = us.FirstOrDefault();
                                u.Note = fs.Name;
                                //這邊ViewData 沒有作用 因為OnActionExecuting...
                                    //filterContext.Controller.ViewData["QuestionTitle"] = fs.Name;
                                    //fc.Controller.ViewData["QuestionTitle"] = fs.Name;
                                var result = new ViewResult
                                {
                                    ViewName="~/Views/FormMaker/AreadyDone.cshtml",
                                     ViewData=new ViewDataDictionary(u)
                                };
                                filterContext.Result = result;
                            }
                        }
                    }

                }
                else
                {
                    fc.Controller.ViewData.Add("Msg", "目前該問卷不存在?");
                    fc.Result = new RedirectToRouteResult(new RouteValueDictionary{
                            {"controller", "FormMaker"},
                            {"action", "OddPage"}
                        });
                }
            }
        }
    }
}