﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FormMaker.Models;

namespace FormMaker.ViewModels {

    public class UserViewModel {
        public string UserID { get; set; }
        public string UserName { get; set; }
    }

    public class FormSetsViewModel {
        public IEnumerable<FormSet> FormSets { get; set; }
    }

    public class FormSetViewModel {
        public UserViewModel UserInfo { set; get; }
        public FormSet FormSet { get; set; }
        public IEnumerable<Fd> Fds { get; set; }
        public IEnumerable<vFdOption> vFdo { get; set; }
    }

}