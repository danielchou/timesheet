﻿目的說明:
1.每個同事都可以發動問卷調查，調查的結果可選擇是要自己看或是提供給大家看。
2.可了解調查結果完成了多少人?
3.每個題目的結果統計是如何?

功能:
*	每個問題拖拉，就可以自動排序
*	問卷設計者可以限定何時啟用活動，過了時間就關閉不能投票。	--20141208
*	每項問卷都有題目上限，超過投票人數就要鎖死題目。

測試:
*	http://localhost:21018/FormMaker/TransferToSurvey?token=D8990AF4-8EA3-4009-806F-9977B0F04E8C&FsID=1fb75d65-5ac0-45f5-9b55-11bb6e721bf5
*	http://localhost:21018/Form/ResultStatic?FsID=1fb75d65-5ac0-45f5-9b55-11bb6e721bf5

ToDos:
*v	新增Question問題壞了還沒修好
*v	排序Question，透過前端拖拉決定在傳回後端對應寫好。
*v	Options of MulitChecks are sortable and accessable from DB.
*v	User may return/review his anwsers and change options again.
*v	主頁說明不夠清楚。使用者有權限決定是否將結果公開與否。
*v	HR的統計結果畫面
*V	StartDate and EndDate, Form'd be closed while expired.
*	連結到Portal Link by user's guid.
--------------------------------------------------------------------------
*	Complitation page after provide user's anwser.
*	Form validation.
*	Clone question item.



