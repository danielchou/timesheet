﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TimeSheatKPI.Models;

namespace TimeSheatKPI.Common {
    
    public class HRMIS {

        HRMISEntities HRMISdb;

        public HRMIS() { 
            
        }

        public void SendingErrMsg(Exception ex) {
            using (HRMISdb = new HRMISEntities()) {
                A_MailList mail = new A_MailList() {
                    FromDB = "TimeSheet-Error",
                    Subject = "[TimeSheet] Issue." + ex.Message,
                    Body = ex.ToString(),
                    CreateTime = DateTime.Now.ToString("yyyy-MM-dd H:mm:ss"),
                    MailFrom = "onelab.mis@onelab.tw",
                    MailTo = "daniel.chou@onelab.tw",
                    mGUID = System.Guid.NewGuid().ToString().Replace("-", "")
                };
                HRMISdb.AddToA_MailList(mail);
                HRMISdb.SaveChanges();
            }
        }

        public void SendingErrMsg(Exception ex, string[] arr) {
            string paras = "";
            foreach (var s in arr) { paras += s + "<br />"; }

            using (HRMISdb = new HRMISEntities()) {
                A_MailList mail = new A_MailList() {
                    FromDB = "TimeSheet-Error",
                    Subject = "[TimeSheet] Issue." + ex.Message,
                    Body = ex.ToString() + "<br />Paras:" + paras,
                    CreateTime = DateTime.Now.ToString("yyyy-MM-dd H:mm:ss"),
                    MailFrom = "onelab.mis@onelab.tw",
                    MailTo = "daniel.chou@onelab.tw",
                    mGUID = System.Guid.NewGuid().ToString().Replace("-", "")
                };
                HRMISdb.AddToA_MailList(mail);
                HRMISdb.SaveChanges();
            }
        }
    
    }
}