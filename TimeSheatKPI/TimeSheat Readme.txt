TimeSheet幾個交代事項：

1.測試後門：
	--DEV
	http://localhost:1791/BackDoor/0/3239BE2945BD904F 
	--PRD
	http://192.168.7.18/TimeSheet/BackDoor/0/3239BE2945BD904F
	
	--檢查發出所有Email套用結果:
	http://localhost:1791/Notify/SendEMail			
	--檢查所有人的Email用一個網頁呈現排列。
	http://localhost:1791/Notify/CheckTodayEmail	
	--檢查在某區段時間內所有人的Email
	http://localhost:1791/Notify/CheckTodayEmail?StartDate=2014-10-05&EndDate=2014-10-15			
	--這段時間內就真的會寄發出去!
	http://localhost:1791/Notify/CheckTodayEmail?StartDate=2014-10-05&EndDate=2014-10-15&mode=mail	


* Task:只留下Execution,Maintenance,Planning三種，其他被JY決議刪除。
* DB多了以下Table,View
	Employee			:TimeSheet內自己維護需要填工時的員工編號。
	vAllEmpWorkingDays	:所有員工對應上班日，從HRMIS直接join進來。
	vDateHrSum			:所有員工每天填寫的總時數，Email判斷用!
	vAllEmpHrInadequate	:所有員工工時不足的提醒名單，Email判斷用!
	fnYYMMDD			:SQL Function 產生類似這樣的時間格式 2014-10-01。
	
* Email功能
	a. 檢核正式輸入資料從10/6開始!
	b. 192.168.5.238 Windows排程直接用CallTimeSheetSendEmail.vbs呼叫網頁去啟動寄信功能
	c. 若要改Email Template 在 ../Views/Shared/NotifyTemplate.htm檔案，直接修改就好!

* Web.config設定
	a. "IsEnableSendMail":若Email有嚴重問題，把Y改成N，就不會再寄信了。
	b. "MailToTester":若要測試Email收件者都是自己，改成自己名字就好，空白為正常寄信者。
