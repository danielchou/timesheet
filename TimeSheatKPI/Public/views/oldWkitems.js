﻿function makeTaskOptions() {
    var ss = "", t, d;
    for (t in rs) { d = rs[t]; ss += "<div class='item' data-value='" + d.TaskID + "'>" + d.TakeName + "</div>"; }
    $(".ui.tasd.dropdown .menu").html(ss);
    $(".ui.task.dropdown").dropdown({
        onChange: function (v) {
            //$(".selectFuncSuccess.label").show();
        }
    });
}


function makeProdOptions(rs) {
    var ss = "", d;
    for (var t in rs) { d = rs[t]; ss += "<div class='item' data-value='" + d + "'>" + d + "</div>"; }
    $('.ui.selection.prod.dropdown').find(".menu").html(ss).andSelf()
            .dropdown({
                onChange: function (vj) {
                    initProjDropDown(vj);
                    $(".selectFuncSuccess.label").hide();
                }
            }).find(".text").html("Selected Product");
}

function makeProjOptions(rs) {
    var ss = "", t, d;
    for (t in rs) { d = rs[t]; ss += "<div class='item' data-value='" + rs[t] + "'>" + rs[t] + "</div>"; }
    $('.ui.selection.proj.dropdown').find(".menu").html(ss).andSelf()
            .dropdown({
                onChange: function (vj) {
                    initFuncDropDown(vd, vj);
                    $(".selectFuncSuccess.label").hide();
                }
            });
    $(".ui.selection.proj.dropdown .text").html("Selected Project");
}


function makeFuncOptions() {
    var ss = "", t, d;
    for (t in rs) { d = rs[t]; ss += "<div class='item' data-value='" + d.PID + "'>" + d.Func + "</div>"; }
    $('.ui.func.dropdown').show().find(".menu").html(ss).andSelf()
            .dropdown({
                onChange: function (v) {
                    //TODO:跳出一個確認符號!
                    $(".selectFuncSuccess.label").show();
                }
            });
}

$(".setting.icon").live("click", function () {
    var me = $(this);
    var titlesn = me.parent().data("titlesn");

    $.post(host + "Ajax/GetWorkingItemDetails", { SN: titlesn }, function (rs) {
        var me = $(".workingItems.modal");
        me.modal("show");
        initProdDropDown();
        initTaskDropDown();

        var d = rs;
        var wkitemStr = "<a class='ui teal label'>{0}</a><a class='ui teal label'>{1}</a><a class='ui teal label'>{2}</a><a class='ui green label'>{3}</a>";
        wkitemStr = String.format(wkitemStr, d.wkitem.Prod, d.wkitem.Proj, d.wkitem.Func, d.task.TaskName);
        //console.log(titlesn, rs, wkitemStr);
        $(".updated.wkitem")
                .attr("data-wktitlesn", titlesn)
                .attr("data-wkitemMode", "Edit")
                .html("Original: " + wkitemStr);
        $(".selection.prod.dropdown .text").html(d.wkitem.Prod);
        $(".selection.proj.dropdown .text").html(d.wkitem.Proj);
        $(".selection.func.dropdown .text").html(d.wkitem.Func).andSelf().find("#selectedFunc").val(d.wkitem.PID);
        $(".selection.task.dropdown .text").html(d.task.TaskName).andSelf().find("#selectedTask").val(d.task.TaskID);
    });
});

$("#SaveWorkingTitle").click(function () {
    wkitem = selectedPID; // $("#selectedFunc").val();
    task = selectedTask; // $("#selectedTask").val();

    var mode = $(".updated.wkitem").attr("data-wkitemmode");
    var wktitlesn = $(".updated.wkitem").attr("data-wktitlesn");
    wktitlesn = (mode == "addnew") ? -1 : wktitlesn;

    //    if (wkitem == "") { alert("請選擇工作項目"); }
    //    else if (task == "") { alert("請選擇Task"); }
    //    else {
    //console.log(mode, wktitlesn, wkitem, task, user, iMMM, currYY);
    $.post(host + "Ajax/SaveWkTitle", { mode: mode, sn: wktitlesn, wkitem: wkitem, task: task, user: user, MM: iMMM, YY: currYY }, function (rs) {
        //console.log(rs);
        if (rs == "repeat") { alert("您已選過了該工作項目!"); } else {
            ShowTimeSheat();
        }
    });
    //}
});